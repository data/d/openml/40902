# OpenML dataset: Shuttle_2percent

https://www.openml.org/d/40902

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Markus Goldstein  
Thanks to NASA for allowing UCL to use the shuttle datasets.  
**Source**: [original](http://www.madm.eu/downloads) - Date unknown  
**Please cite**: Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.  

&quot;The shuttle dataset describes radiator positions in a NASA space shuttle with 9 attributes and was designed for supervised anomaly detection. Besides the normal &ldquo;radi- ator flow&rdquo; class, about 20% of the original data describe abnormal situations. To reduce the number of anomalies, we select the class 1 as normal and apply a stratified sampling for the classes 2, 3, 5, 6 and 7 [...]. Training and test set are combined in a single big dataset, which has as a result 46,464 instances with 1.89% anomalies.&quot;(cite from Goldstein, Markus, and Seiichi Uchida. &quot;A comparative evaluation of unsupervised anomaly detection algorithms for multivariate data.&quot; PloS one 11.4 (2016): e0152173.).  This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40902) of an [OpenML dataset](https://www.openml.org/d/40902). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40902/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40902/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40902/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

